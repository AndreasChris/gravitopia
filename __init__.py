from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputController
from st3m.utils import tau

import math
import leds

import st3m.run

# Gravitopia Game by Andreas Chris Wilhelmer [Work in Progress]
# Published under AGPL-3.0-or-later

class Gravitopia(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.input = InputController()
        self.pos_x = [10.0, -10.0, 0.0]
        self.pos_y = [0.0, 0.0, 10.0]
        self.vel_x = [0.0, 0.0, 0.0]
        self.vel_y = [0.0, 0.0, 0.0]
        self.gravity_rotation = [0.0, math.pi, math.pi / 2.0]
        self.gravity_indicator_x = [120.0, -120.0, 0.0]
        self.gravity_indicator_y = [120.0, -120.0, -120.0]
        self.gravity_indicator_speed = 2.0
        self.acc = 50.0
        self.petal_id_left = [4, 8, 1]
        self.petal_id_right = [2, 6, 9]
        self.petal_id_special = [3, 7, 0]
        self.petal_last_pressed = [math.inf]*10
        
        leds.set_all_rgb(1.0,1.0,1.0)
        leds.set_brightness(0)
        leds.update()


    def draw(self, ctx: Context) -> None:
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(255, 0, 0).arc(self.pos_x[0], -self.pos_y[0], 6, 0, tau, 0).fill()
        ctx.rgb(255, 0, 0).arc(self.gravity_indicator_x[0], self.gravity_indicator_y[0], 6, 0, tau, 0).fill()
        for i in range(8,17):
            leds.set_rgb(i,1.0,0.0,0.0)
        ctx.rgb(0, 0, 255).arc(self.pos_x[1], -self.pos_y[1], 6, 0, tau, 0).fill()
        ctx.rgb(0, 0, 255).arc(self.gravity_indicator_x[1], self.gravity_indicator_y[1], 6, 0, tau, 0).fill()
        for i in range(24,33):
            leds.set_rgb(i,0.0,0.0,1.0)
        ctx.rgb(0, 255, 0).arc(self.pos_x[2], -self.pos_y[2], 6, 0, tau, 0).fill()
        ctx.rgb(0, 255, 0).arc(self.gravity_indicator_x[2], self.gravity_indicator_y[2], 6, 0, tau, 0).fill()
        for i in range(0,6):
            leds.set_rgb(i,0.0,1.0,0.0)
        for i in range(35,40):
            leds.set_rgb(i,0.0,1.0,0.0)
        leds.set_brightness(255)
        leds.update()

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)

        for i in [0,1,2]:
            if ins.captouch.petals[self.petal_id_left[i]].pressed:
                self.gravity_rotation[i] -= self.gravity_indicator_speed *delta_ms / 1000
                self.petal_last_pressed[self.petal_id_left[i]] = 0.0
            else:
                self.petal_last_pressed[self.petal_id_left[i]] += delta_ms
            if ins.captouch.petals[self.petal_id_right[i]].pressed:
                self.gravity_rotation[i] += self.gravity_indicator_speed * delta_ms / 1000
                self.petal_last_pressed[self.petal_id_right[i]] = 0.0
            else:
                self.petal_last_pressed[self.petal_id_right[i]] += delta_ms
            if ins.captouch.petals[self.petal_id_special[i]].pressed:
                if self.petal_last_pressed[self.petal_id_special[i]] > 1000:
                    self.gravity_rotation[i] += math.pi
                self.petal_last_pressed[self.petal_id_special[i]] = 0.0
            else:
                self.petal_last_pressed[self.petal_id_special[i]] += delta_ms
            self.vel_x[i] += math.cos(self.gravity_rotation[i]) * self.acc * delta_ms / 1000
            self.vel_y[i] += math.sin(self.gravity_rotation[i]) * self.acc * delta_ms / 1000
            self.gravity_indicator_x[i] = math.cos(self.gravity_rotation[i])*120
            self.gravity_indicator_y[i] = -math.sin(self.gravity_rotation[i])*120

            x = self.pos_x[i] + self.vel_x[i] * delta_ms / 1000.0
            y = self.pos_y[i] + self.vel_y[i] * delta_ms / 1000.0

            if x**2 + y**2 - (120 - 6) ** 2 < 0:
                self.pos_x[i] = x
                self.pos_y[i] = y
            else: #calculate bounceback angle
                corrpos_x = x
                corrpos_y = y
                corrvel_x = self.vel_x[i]
                corrvel_y = self.vel_y[i]

                tangent_normvec_len = math.sqrt(x**2 + y**2)
                tangent_normvec_x = x / tangent_normvec_len
                tangent_normvec_y = y / tangent_normvec_len

                projection_posvec_tangent = (self.vel_x[i] * tangent_normvec_x) + (self.vel_y[i] * tangent_normvec_y)

                self.vel_x[i] -= 2 * projection_posvec_tangent * tangent_normvec_x
                self.vel_y[i] -= 2 * projection_posvec_tangent * tangent_normvec_y

            #degrade speed over time
            self.vel_x[i] *= math.pow(0.9, delta_ms / 1000)
            self.vel_y[i] *= math.pow(0.9, delta_ms / 1000)

if __name__ == '__main__':
    st3m.run.run_view(Gravitopia(ApplicationContext()))

